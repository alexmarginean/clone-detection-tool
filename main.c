//
//  main.c
//  CloneDetector
//
//  Created by Marginean Alexandru on 20/03/14.
//  Copyright (c) 2014 Marginean Alexandru. All rights reserved.
//


#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


char * typeOfClone = NULL;
char * minimalCloneThreshold = NULL;
char * maxDistanceType3 = NULL;
char * finalOutputPath = NULL;


//set default values: type 3b, minimal clone thresgold 3, max distance for type 3 6
void setDefaultValues(){
    if(typeOfClone == NULL){
        typeOfClone = (char*)malloc(10* sizeof(char));
        strcpy(typeOfClone, "3b");
    }
    if (minimalCloneThreshold == NULL) {
        minimalCloneThreshold = (char*)malloc(10* sizeof(char));
        strcpy(minimalCloneThreshold, "3");
    }
    if(maxDistanceType3 == NULL){
        maxDistanceType3 = (char*)malloc(10*sizeof(char));
        strcpy(maxDistanceType3, "6");
    }
}


//free the memory
void freeMemory(){
    free(typeOfClone);
    free(minimalCloneThreshold);
    free(maxDistanceType3);
    free(finalOutputPath);
}


int main(int argc,  char * argv[])
{
    const char * file1 = argv[1];
    const char * file2 = argv[2];
    //there should be a even number of arguments + the number of the file, so odd
    //minimal arguments are the name of the two files
    if (argc %2 == 0 || argc < 3){
        printf("Wrong number of arguments!");
        return 0;
    }
    //parse the arguments and set the corresponding values
    for(int i = 3; i < argc; i = i + 2){
        if(!strcmp(argv[i], "-type")){
            typeOfClone = argv[i+1];
            if (!strcmp(typeOfClone, "1") || ! strcmp(typeOfClone, "2")) {
                maxDistanceType3 = (char*)malloc(10*sizeof(char));
                strcpy(maxDistanceType3, "1");
            }
        }
        else if (!strcmp(argv[i], "-threshold")){
            minimalCloneThreshold = argv[i+1];
        }
        else if(!strcmp(argv[i], "-maxD")){
            maxDistanceType3 = argv[i+1];
        }
        else if(!strcmp(argv[i], "-o")){
            finalOutputPath = argv[i+1];
        }
    }
    //set default Values
    setDefaultValues();
    char command [250];
    char command2[250];
    //all different commands for different clone types
    char commandType1 [250];
    char commandType2 [250];
    char commandType3a [250];
    char commandType3b [250];
    //default outputs
    char * defaultOutputType1 = "output/Type1Clones.out";
    char * defaultOutputType2 = "output/Type2Clones.out";
    char * defaultOutputType3a = "output/Type3aClones.out";
    char * defaultOutputType3b = "output/Type3bClones.out";
    //if the final output was not received as an argument, output the results into the default location
    if(finalOutputPath == NULL){
        sprintf(commandType1, "bin/./CloneDetectorInstantiatedIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2, minimalCloneThreshold, "1", defaultOutputType1);
        sprintf(commandType2, "bin/./CloneDetectorAbstractIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2, minimalCloneThreshold, "1", defaultOutputType2);
        sprintf(commandType3a, "bin/./CloneDetectorInstantiatedIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2,  minimalCloneThreshold, maxDistanceType3, defaultOutputType3a);
        sprintf(commandType3b, "bin/./CloneDetectorAbstractIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2, minimalCloneThreshold, maxDistanceType3, defaultOutputType3b);
    }
    //else output the results into the received place
    else{
        sprintf(commandType1, "bin/./CloneDetectorInstantiatedIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2, minimalCloneThreshold, "1", finalOutputPath);
        sprintf(commandType2, "bin/./CloneDetectorAbstractIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2, minimalCloneThreshold, "1", finalOutputPath);
        sprintf(commandType3a, "bin/./CloneDetectorInstantiatedIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2,  minimalCloneThreshold, maxDistanceType3, finalOutputPath);
        sprintf(commandType3b, "bin/./CloneDetectorAbstractIDS.x %s %s -q -o /dev/null %s %s %s", file1, file2, minimalCloneThreshold, maxDistanceType3, finalOutputPath);
    }
    //set the final command, according to the desired type of clone
    char * finalComand;
    if (!strcmp(typeOfClone,"1")) {
        finalComand = commandType1;
    }
    else if (!strcmp(typeOfClone, "2")){
        finalComand = commandType2;
    }
    else if (!strcmp(typeOfClone, "3a")){
        finalComand = commandType3a;
    }
    else if (!strcmp(typeOfClone, "3b")){
        finalComand = commandType3b;
    }
    else{
        printf("Wrong Clone Type!");
        return 0;
    }
    sprintf(command,"%s", finalComand);
    sprintf(command2,"%s", "python bin/countLines.py Temporary/original.out Temporary/clonesLOCs.out");
    int status = system(command);
    if(status != 0){
        printf("ERROR");
    }
    int status2 = system(command2);
    if(status2 != 0){
        printf("ERROR");
    }
    return 0;
}

