//
//  main.c
//  TestHarness
//
//  Created by Marginean Alexandru on 25/03/14.
//  Copyright (c) 2014 Marginean Alexandru. All rights reserved.
//

#include <stdio.h>

#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


//read the entire content of the input folder
void readDirContent (char * path, char **ListOfFiles, int * pos)
{
    DIR           *d;
    struct dirent *dir;
    char * newPath;
    newPath = (char*)malloc(500*sizeof(char));
    strcpy(newPath,path);
    strcat(newPath, "/");
    d = opendir(path);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            //if it is a folder go recursive
            if(dir->d_type == 4 && strcmp(".", dir->d_name) && strcmp("..", dir->d_name)){
                readDirContent(strcat(newPath,dir->d_name), ListOfFiles, pos);
            }
            //if it is a file, add it to the list of input files
            else if(dir->d_name[strlen(dir->d_name)-1] == 'n'){
                strcpy(ListOfFiles[*pos], dir->d_name);
                (*pos)++;
            }
        }
        closedir(d);
    }
}


//the test function
void test(char ** Files, int pos, char * type, int threshold, int maxD, char * outputFile){
    char outputFileName [250];
    //just the default report file
    sprintf(outputFileName, "output/SimilarityOutput/%s", outputFile);
    FILE * fout = fopen(outputFileName,"w");
    //LATEX header of the table
    fprintf(fout, "\\begin{longtable}{| p{.015\\textwidth} | p{.20\\textwidth} | p{.20\\textwidth}  | p{.10\\textwidth} | p{.10\\textwidth} | p{.10\\textwidth} | p{.10\\textwidth} |} \\hline  \n       \\textbf{No.\\cellcolor{gray}  }  & \\textbf{Source\\cellcolor{gray}  }  &  \\textbf{CloneFile}\\cellcolor{gray} &  \\textbf{Type} \\cellcolor{gray} &  \\textbf{Thresh\\cellcolor{gray} } &  \\textbf{MaxD\\cellcolor{gray} } &\\textbf{Result\\cellcolor{gray} }  \\\\ \\hline\n");
    int number = 1;
    //sum of similarities for outputting the avarage
    double sumOfSimilarities = 0;
    //for every file in the input list
    for(int i = 0; i < pos; i++){
        //compare it with all the others, including itself for sanity check
        for (int j = 0; j<pos; j++) {
            FILE *fp;
            char path[1035];
            char command [250];
            //call the clone detector
            sprintf(command, "./CloneDetectorMain input/%s input/%s -type %s -threshold %d -maxD %d", Files[i], Files[j], type, threshold, maxD);
            fp = popen(command, "r");
            if (fp == NULL) {
                printf("Failed to run command\n" );
                //exit;
            }
            //read the output from stdin
            while (fgets(path, sizeof(path)-1, fp) != NULL) {
            }
            //convert the output into double
            double d;
            sscanf(path, "%lf", &d);
            sumOfSimilarities = sumOfSimilarities + d;
            char result[1000];
            //add a new line to the latex table
            sprintf(result, "%d\\cellcolor{yellow} & %s & %s & 1 & 3  & %d & %.4f\\\\ \\hline\n", number, Files[i], Files[j], maxD, d);
            fprintf(fout, result);
            number ++;
            
        }
    }
    //the avarage similarity
    double avarageSimilarity = sumOfSimilarities / (number - 1);
    //output it as a new line in the table
    fprintf(fout, "& Similarity =   \\cellcolor{green}& %.4f \\cellcolor{green} & & & & \\\\ \\hline \n", avarageSimilarity);
    //the ending of the LATEX table
    fprintf(fout,"\\caption{Type: %s - Threshold: %d MaxD: %d } \n \\label{tab:myfirstlongtable}\\end{longtable}", type, threshold, maxD);
    fclose(fout);
}


//the main function
int main(int argc, const char * argv[])
{
    //the list of files to be compared
    char **ListOfFiles;
    ListOfFiles = (char**)malloc(250 * sizeof(char*));
    //allocate it
    for(int i = 0; i < 250; i ++){
        ListOfFiles[i] = (char*)malloc(250*sizeof(char));
    }
    int *pos;
    pos = (int*)malloc(sizeof(int));
    *pos = 0;
    //the default path is the input folder
    char *path = "input";
    //read the dirr content and run the matching for different settings
    readDirContent(path,ListOfFiles,pos);
    //threshold 3
    test(ListOfFiles, *pos, "1", 3, 1, "type1thres3.out");
    test(ListOfFiles, *pos, "2", 3, 1, "type2thres3.out");
    test(ListOfFiles, *pos, "3a", 3, 6, "type3athres3Maxd6.out");
    test(ListOfFiles, *pos, "3b", 3, 6, "type3bthres3Maxd6.out");
    //threshold 6
    test(ListOfFiles, *pos, "1", 6, 1, "type1thres6.out");
    test(ListOfFiles, *pos, "2", 6, 1, "type2thres6.out");
    test(ListOfFiles, *pos, "3a", 6, 6, "type3athres6Maxd6.out");
    test(ListOfFiles, *pos, "3b", 6, 6, "type3bthres6Maxd6.out");
    //just free the memory
    free(pos);
    for(int i = 0; i < 250; i ++){
        free(ListOfFiles[i]);
    }
    free(ListOfFiles);
    return 0;
}

