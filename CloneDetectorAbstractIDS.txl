include "java.grm"


%grammar definitions


%add filename for output at method or constructor declaration
redefine method_or_constructor_declaration
	[srcfilename] ...
end redefine


%grammar definition for what a clone output may contain
define method_or_repeat_statements
	[method_or_constructor_declaration]
	|	[repeat declaration_or_statement]
	|	[field_declaration]
	|	[type_declaration]
	|	[import_declaration]
	|	[class_body_declaration]
	|	[repeat id]
	|	[class_header]
	|	[interface_header]
end define


%file marker for output
define fileMarker
	'IN FILE:  
	|	[empty]
end define


%end marker for output
define endMarker
	END
	|	[empty]
end define


%original version marker for output
define originalVersionMarker
	ORIGINAL VERSION 'IN FILE:  
end define


%clone list marker for output
define cloneListMarker
	CLONES LIST:
end define


%cloneOutput is the grammar definition for a represantation in output file of an individual clone
define cloneOutput
	[NL] [IN]  [opt fileMarker] [opt srcfilename] [opt stringlit] [NL] [NL] [IN] [method_or_repeat_statements] [NL] [EX] [opt endMarker] [opt srclinenumber] [EX] [NL] [NL] [NL]
end define


%clone header is the version of the clone in the original file
define cloneHeader
	[originalVersionMarker] [opt srcfilename] [NL] [IN][IN][method_or_repeat_statements] [NL] [NL] [NL] [EX] [EX][cloneListMarker] [NL] 
end define


%a class of clones represents the output of an original source code and of all its clones
define classOfClones
	[cloneHeader] [repeat cloneOutput]
end define


%begin clone marker for LCS output
define beginCloneMarker
	BEGINCLONE
end define


%end clone marker for LCS output
define endClonemarker
	ENDCLONE
end define


%an LCSClone is the output of a clone from the LCS script
define LCSClone
	[beginCloneMarker] [NL] [repeat stringlit] [endClonemarker] [NL] 
	|	[empty]
end define


%LCS Output is the entire grammar for parsing an LCS script's output
define LCSOutput
	[repeat LCSClone]
	|	[empty]
end define




% transformations rules


%main function is the entry point of a TXL program
%parse the input file, read the cloned file and the arguments from command line
function main
    match [program]
		P [program] 
	import TXLargs [repeat stringlit]
	%arguments are threshold, max Distance, and the output file
	deconstruct TXLargs
		OtherFileName [stringlit] CloneThreshold [stringlit] Type3MaxDistance [stringlit] OutputFile [stringlit] rest [repeat stringlit]
	export CloneThreshold
	export Type3MaxDistance
	export OutputFile
	construct SecondProgram [program]
		_ [read OtherFileName] 
	%pretty print the cloned file, for later usage in similiarity precent 
	construct SecondProgramForCounting [program]
		SecondProgram [removeFileNameForMatching]  [write "Temporary/original.out"]	
	construct ClonedClassesList [repeat type_declaration]
	export ClonedClassesList
	%run the clone matching section
	construct runCloneMatching [id]
		_ [deconstructPrograms P SecondProgram]
end function



%this function will deconstruct the original program, into all its components
%and then it will parse all this individual components for future analyse
function deconstructPrograms	FirstProgram [program]
								SecondProgram [program]
	replace [id]
		current [id]
	%deconstruct both programs into package declaration
	deconstruct  FirstProgram
		FirstProgramPackDecl [package_declaration]
	deconstruct SecondProgram
		SecondProgramPackDecl [package_declaration]
	%extract all classes, field declarations, imports, interfaces and enum declarations from both files
	%field declarations, imports and enum declarations will not enter in the clone matching alghorithm
	%and will not require an thrashold
	%them will just be compared one by one, and if there is a match it will be added at the similiarity percentage
	%since usually this are just individual LOCs it is not point in displaying them as clones
	construct AllClassesFromFirstFile [repeat type_declaration]
		_ [^ FirstProgram]
	construct AllClassesFromSecondFile [repeat type_declaration]
		_ [^ SecondProgram] 
	construct FirstProgramFieldDecl [repeat field_declaration]
		_ [^ FirstProgram]
	construct SecondProgramFieldDecl [repeat field_declaration]
		_ [^ SecondProgram] 
	construct AllImportsFromFirstFile [repeat import_declaration]
		_ [^ FirstProgram]
	construct AllImportsFromSecondFile [repeat import_declaration]
		_ [^ SecondProgram]
	construct AllInterfaceDeclFromFirstFile [repeat interface_declaration]
		_ [^ FirstProgram]
	construct AllInterfaceDeclFromSecondFile [repeat interface_declaration]
		_ [^ SecondProgram]
	construct AllEnumDeclFromFirstFile [repeat enum_declaration]
		_ [^ FirstProgram]
	construct AllEnumDeclFromSecondFile [repeat enum_declaration]
		_ [^ SecondProgram]
	construct AllClassHeadersFromFirstFile [repeat class_header]
		_ [^ FirstProgram]
	construct AllClassHeadersFromSecondFile [repeat class_header]
		_ [^ SecondProgram]
	construct AllInterfaceHeadersFromFirstFile [repeat interface_header]
		_ [^ FirstProgram]
	construct AllInterfaceHeadersFromSecondFile [repeat interface_header]
		_ [^ SecondProgram]
	%we ignore package header	
	%the first step is to match all class, interface and enum level clones
	construct matchAllClones [repeat classOfClones]
		_ [matchClassClones AllClassesFromFirstFile AllClassesFromSecondFile]
	%take the obtained class level clones, for removing them for future matching
	import ClonedClassesList [repeat type_declaration] 
	%remove them
	construct programWithoutClonedClasses [program]
		SecondProgram [removeCloneClasses ClonedClassesList] 
	import OutputFile [stringlit]
	%for the rest of the source code, which was not matched as class, interface or enums clone
	%match clone at methods level and statement level
	%and start with the list of already obtained clones
	%print this list as a clone output, in the output file
	%so, the output file will ignore packet headers, include file, and field declarations
	construct ClonesListFromMethodsAndStatements [repeat classOfClones]
		matchAllClones [matchClones FirstProgram programWithoutClonedClasses] [write OutputFile]
	%construct the list of cloned fields, for similiarity percentage
	construct ClonedFieldsList [repeat cloneOutput]
		_ [matchClonedField FirstProgramFieldDecl SecondProgramFieldDecl] 
	%construct the list of cloned imports, for similiarity percentage
	construct ClonedIncludesList [repeat cloneOutput]
		_ [matchClonedIncludes AllImportsFromFirstFile AllImportsFromSecondFile]
	%construct the list of cloned Class Headers, for similiarity percentage
	construct ClonedClassHeadersList [repeat cloneOutput]
		_ [matchClassHeader AllClassHeadersFromFirstFile AllClassHeadersFromSecondFile]
	%construct the list of cloned Interface Headers, for similiarity percentage
	construct ClonedInterfaceHeadersList [repeat cloneOutput]
		_ [matchClonedInterface AllInterfaceHeadersFromFirstFile AllInterfaceHeadersFromSecondFile]
	%construct the list of cloned imports and fields, and the rest of LOCs from other identified clones
	%for similiarity percentage, and write them to Temporary/clonesLOCs.out, for usage of Line Counter script
	construct ListOfClonesForCounting [repeat cloneOutput]
		_ [addJustLinesFromClones ClonesListFromMethodsAndStatements] [. ClonedIncludesList]
	construct CloneListForCounting [repeat cloneOutput]
		ListOfClonesForCounting [. ClonedFieldsList] [. ClonedClassHeadersList] [.ClonedInterfaceHeadersList] [write "Temporary/clonesLOCs.out"]
	by
		current
end function


%just replace all type_declarations with nothing, if they were matched as a clone
rule removeCloneClasses CloneList [repeat type_declaration]
	replace [type_declaration]
		current [type_declaration]
	where
		CloneList [classIsInList current]
	by
		%notrhing
end rule


%match the current class in the list of current clone matched classes
function classIsInList currentClass [type_declaration]
	match * [type_declaration]
		currentInList [type_declaration]
	where
		currentInList [= currentClass]
end function


%match type declaration level clones - class, interface end enums
function matchClassClones	FirstProgramClass [repeat type_declaration]
							SecondProgrmClass [repeat type_declaration]
	replace [repeat classOfClones]
		ListOfClones [repeat classOfClones]
	%take the first entry in the main program
	deconstruct FirstProgramClass 
		firstClass [type_declaration] rest [repeat type_declaration]
	%remove file name for matching
	construct originalFirstClassWithoutFileName [type_declaration]
		firstClass [removeFileNameForMatching]
	%take the filename for output 	
	deconstruct * [srcfilename] firstClass
		firstClassFileName [srcfilename]
	%construct a clone header	
	construct myCloneHeader [cloneHeader]
		ORIGINAL VERSION 'IN FILE:   firstClassFileName originalFirstClassWithoutFileName CLONES LIST: 
	%now parse the current type definition, for matching in the second program
	construct ClonedMethods [repeat cloneOutput]
		_ [deconstructSecondprogramClassList SecondProgrmClass firstClass]
	%construct a class of clone from the header and the current obtained clones		
	construct myClassOfClone [classOfClones]
		myCloneHeader ClonedMethods
	%add the new class just if it contains cloned method
	construct newListOfClones [repeat classOfClones]
		ListOfClones [constructNewListOfClonesIfNotEmpty myClassOfClone]
	by
		newListOfClones
		%continue with the rest of methods
		[matchClassClones rest SecondProgrmClass]
		
end function


%for every type definition in the main program, try matches with all type definitions from second program
function deconstructSecondprogramClassList	SecondProgram [repeat type_declaration]
											FirstClass [type_declaration]
	replace [repeat cloneOutput]
		currentOutput [repeat cloneOutput]
	deconstruct SecondProgram
		first [type_declaration] rest [repeat type_declaration]
	%add the class just if it is a clone			
	construct ClonedMethods [repeat cloneOutput]
		_ [matchIfClassIsClone FirstClass first]

	by
		currentOutput [. ClonedMethods]
		[deconstructSecondprogramClassList rest FirstClass]
end function


%this method will match two type declaration if they are clones
function matchIfClassIsClone	V1 [type_declaration]
								V2 [type_declaration]
	replace  [repeat cloneOutput]
		AreClones [repeat cloneOutput]
	%remove line numbers and file names for matching, and keep the source file name for matching
	construct FirstProgramClass [type_declaration]
		V1  [removeLineNumbersForMatching] [removeFileNameForMatching] [normalize]
	construct SecondProgramClass [type_declaration]
		V2 	[removeLineNumbersForMatching] [removeFileNameForMatching] [normalize]
	deconstruct * [srcfilename] V2
		CloneFileName [srcfilename]
	%remove the file name for outputs
	construct cloneWithoutFileName [type_declaration]
		V2 [removeFileNameForMatching]
	construct classLevelMarker [stringlit]
		"Class Level Clone"
	%a new clone output
	construct newClone [cloneOutput]
		'IN FILE:   CloneFileName classLevelMarker cloneWithoutFileName END
	where
		%this replacement must be done just if the two type_definitions are exactly the same
		FirstProgramClass [= SecondProgramClass]	
		%if they are the same, add the type_declaration as a clone in the clones list
		import ClonedClassesList [repeat type_declaration]
		export ClonedClassesList
			ClonedClassesList [. V2]				
	by 
		newClone
end function


%match function and block level clone
function matchClones	FirstProgramm [program]
						SecondProgram [program]
	replace [repeat classOfClones]
		currentOutput [repeat classOfClones]
	%deconstruct the method or constructor into class_body_declaration
	construct FirstProgramMethodsNoEmpty [repeat class_body_declaration]
		_ [^ FirstProgramm]	
	construct SecondProgramMethodsNoEmpty [repeat class_body_declaration]
		_ [^ SecondProgram] 
	%construct the list of clones, at methods and statement level
	construct ListOfClonedMethods [repeat classOfClones]
		_ [deconstructMainProgram FirstProgramMethodsNoEmpty SecondProgramMethodsNoEmpty]
	by
		currentOutput [. ListOfClonedMethods]
end function


%deconstruct the list of body declarations from the original program, construct the clone class, and parse every individual
%element for matching with the cloned program
function deconstructMainProgram	currentListOfMethods [repeat class_body_declaration] SecondProgramMethods [repeat class_body_declaration]
	replace [repeat classOfClones]
		ListOfClones [repeat classOfClones]
	deconstruct currentListOfMethods
		first [class_body_declaration] rest [repeat class_body_declaration]
	deconstruct * [srcfilename] first
		OriginalMethFileName [srcfilename]
	construct cloneWithoutFileName [class_body_declaration]
		first [removeFileNameForMatching]
	construct myCloneHeader [cloneHeader]
		ORIGINAL VERSION 'IN FILE:   OriginalMethFileName cloneWithoutFileName CLONES LIST: 
	%send the current class body declaration from the first program, the the matching function with all class body
	%declarations from the cloned program
	construct ClonedMethods [repeat cloneOutput]
		_ [deconstructSecondProgram SecondProgramMethods first]
	%construct the class of clones and add it just if it contains clones
		
	construct myClassOfClone [classOfClones]
		myCloneHeader ClonedMethods
	construct newListOfClones [repeat classOfClones]
		ListOfClones [constructNewListOfClonesIfNotEmpty myClassOfClone]
	by
		newListOfClones
		[deconstructMainProgram rest SecondProgramMethods]
end function


%add a new class of clone just if it is not empty
function constructNewListOfClonesIfNotEmpty	V1 [classOfClones]
	replace [repeat classOfClones]
		current [repeat classOfClones]
	deconstruct * [repeat cloneOutput] V1
		currentClones [repeat cloneOutput]
	construct nrOfClones [number]
		_ [length currentClones]
	where
		nrOfClones [> 0]
	by
		current [. V1]
end function


%for every method in the main program, match it with all other methods in the cloned program
function deconstructSecondProgram	currentListOfMethods [repeat class_body_declaration] MainProgramCurrent [class_body_declaration]
	replace [repeat cloneOutput]
		ListOfClones [repeat cloneOutput]
	deconstruct currentListOfMethods
		first [class_body_declaration] rest [repeat class_body_declaration]
	%match if the entire method is a clone
	construct ClonedMethods [repeat cloneOutput]
		_ [matchIfItIsClone MainProgramCurrent first]
	%if not, go to statement level, for applying LCS
	construct CloneMethodsAndStatmentLevel [repeat cloneOutput]
		ClonedMethods [constructStatementLevelClones MainProgramCurrent first]
	where not
		%just where the current main program method is not a field declaration
		%that is a separate case
		MainProgramCurrent [matchFieldDeclaration]
	by
		ListOfClones [. CloneMethodsAndStatmentLevel]
		[deconstructSecondProgram rest MainProgramCurrent]
end function


%check if the class body declaration is a field declaration
function matchFieldDeclaration
	match   [class_body_declaration]
		current [class_body_declaration]
	%this deconstruct will fail for all other grammar structures, excepting field declaration
	deconstruct current
		 fn [srcfilename] fieldDecl [field_declaration] 
end function


%when the entire method is not a clone, search for LCSs in the two methods' bodys
function constructStatementLevelClones	MainProgramMethod [class_body_declaration]
										SecondProgramMethod [class_body_declaration]
	replace [repeat cloneOutput]
		CurrentList [repeat cloneOutput]
	construct currentListLength [number]
		_ [length CurrentList]
	where
		%do all this, just if the method was not already matched as a clone
		currentListLength [= 0]
		%first print the list of original sourceFile for outputing the differences
		construct writeSourceOriginal [repeat class_body_declaration]
			MainProgramMethod [write "Temporary/originalSource.in"]
		%normalize before sending it to LCS
		construct writeSource [repeat class_body_declaration]
			MainProgramMethod [normalize] [write "Temporary/source.in"]
		construct writeOriginal [repeat class_body_declaration]
			SecondProgramMethod [write "Temporary/originalDest.in"]
		%normalize before sending it to LCS
		construct writeDest [repeat class_body_declaration]
			SecondProgramMethod [normalize] [write "Temporary/dest.in"]
		deconstruct * [srcfilename] SecondProgramMethod
			cloneFileName [srcfilename]
		import CloneThreshold [stringlit]
		import Type3MaxDistance [stringlit]
		%call the python script for determining all common sequences larger than the thrashold
		%and respecting the max dist between clone's LOCs for type 4 (1 for type 1/2)
		construct Command [stringlit]
			_ [+ "python bin/LCSNormalize.py Temporary/source.in Temporary/dest.in Temporary/originalDest.in Temporary/originalSource.in "] [+ CloneThreshold] [+ " "] [+ Type3MaxDistance] 
		construct CallCommand [id]
    		_ [system Command]
    	%read the output from LCS as LCSOutput
    	construct outputFromLCS [repeat LCSOutput]
    		_ [read "Temporary/output.out"]
    	%construct LCSClone from LCSOutput
		construct listOfClonesOutput [repeat LCSClone]
			_ [constructClonesFromOutput outputFromLCS]
    	construct cloneResult [repeat cloneOutput]
    		_ [addClones listOfClonesOutput cloneFileName]  
	by
		cloneResult
end function


%construct LCSClone  from LCSOutput
function constructClonesFromOutput	V1[repeat LCSOutput]
	replace [repeat LCSClone]
		_ [repeat LCSClone]
	deconstruct  V1
    	listOfClonesOutput [repeat LCSClone]
	by
		listOfClonesOutput
end function


%add the LCSClone to a list of LCSOutput
function addClones	V1 [repeat LCSClone]
					CloneFileName [srcfilename]
	replace [repeat cloneOutput]
		currentOutput [repeat cloneOutput]
	deconstruct V1
		first [LCSClone] rest [repeat LCSClone]
	deconstruct * [repeat stringlit] first
		currentClone [repeat stringlit]
	%add just if it is not empty, with the filename too
	construct cloneResult [repeat cloneOutput]
    	_ [addNonEmptyClone currentClone  CloneFileName]  
	by
		currentOutput [. cloneResult]
		[addClones rest CloneFileName]
end function


%add clones, just if the list of input is not empty
%and add the file name too
function addNonEmptyClone	V1 [repeat stringlit]
							CloneFileName [srcfilename]
	replace [repeat cloneOutput]
		_ [repeat cloneOutput]
	%transform the stringlits in the LCS Output into ID, for cloneOutput
	construct OutputAsID [repeat id]
		_ [constructIdFromStringlit V1]
	construct statementLevelMarker [stringlit]
		"Statement Level Clone"
	%construct a new cloneOutput from the output as ID, including filename
	construct newClone [cloneOutput]
		'IN FILE:   CloneFileName statementLevelMarker OutputAsID END
	construct stringClone [stringlit]
		_ [quote V1]
	construct lengthOfV1 [number]
		_ [# stringClone] 
	where 
		lengthOfV1 [> 1]		
	by
		newClone
end function


%construct a repeat of ids from a repeat of stringlits
function constructIdFromStringlit	listOfStrings[repeat stringlit]
	replace [repeat id]
		current [repeat id]
	deconstruct listOfStrings
		first [stringlit] rest [repeat stringlit]
	construct newID [id]
		_ [unquote first] 
	by
		current [. newID]
		[constructIdFromStringlit rest]
end function


%match two class body declaration as clones
function matchIfItIsClone	V1 [class_body_declaration]
							V2 [class_body_declaration]
	replace  [repeat cloneOutput]
		AreClones [repeat cloneOutput]
	%remove file name and line numbers for matching, and also normalize it
	construct NormalizedFirstProgramMethod [class_body_declaration]
		V1  [removeLineNumbersForMatching] [removeFileNameForMatching] [normalize]
	construct NormalizedSecondProgramMethod [class_body_declaration]
		V2 	[removeLineNumbersForMatching] [removeFileNameForMatching] [normalize]
	%keep filename for output
	deconstruct * [srcfilename] V2
		CloneFileName [srcfilename]
	construct cloneWithoutFileName [class_body_declaration]
		V2 [removeFileNameForMatching]
	construct methodLevelClones [stringlit]
		"Method Level Clones"
	%construct a new cloneOutput, keeping the file name too
	construct newClone [cloneOutput]
		'IN FILE:   CloneFileName methodLevelClones cloneWithoutFileName END
	where
		NormalizedFirstProgramMethod [= NormalizedSecondProgramMethod]					
	by 
		newClone
end function


%just remove any filename with nothing
rule removeFileNameForMatching
	replace $ [srcfilename]
		_ [srcfilename]
	by
		%nothing
end rule


%just remove any srclinenumber with nothing
rule removeLineNumbersForMatching
	replace [srclinenumber]
		current [srclinenumber]
	construct replacement [stringlit]
		_ [quote current]
	construct lengthOfCurrent [number]
		_ [# replacement]
	deconstruct * [srclinenumber] current
		nr [srclinenumber]
	where
		lengthOfCurrent [> 0]
	by
end rule


%this function will match includes as clones, for similiarity percentage
%it will match every individual import line as clone, if it is contained in the original program,
%without considering any thrashold, but it will not output it as a clone
function matchClonedIncludes	FirstProg [repeat import_declaration]
								SecondProg [repeat import_declaration]
	replace [repeat cloneOutput]
		current [repeat cloneOutput]
	deconstruct SecondProg 
		first [import_declaration] rest [repeat import_declaration]
	%add it as a clone, just if it is matched in the first program include list
	construct newCurrentInclude [repeat cloneOutput]
		current [addIncludeIfMatched FirstProg first]
	by
		newCurrentInclude
		[matchClonedIncludes FirstProg rest]
end function


%match an import in the list of the original program's imports
function addIncludeIfMatched	ListOfImports [repeat import_declaration]
								CurrentImport [import_declaration]
		replace [repeat cloneOutput]
			current [repeat cloneOutput]
		construct CloneOutput [cloneOutput]
			CurrentImport
		%normalize before matching
		construct NormalizedListOfImports [repeat import_declaration]
			ListOfImports [normalize] 
		construct NormalizedCurrentImport [import_declaration]
			CurrentImport [normalize]
		where 
			NormalizedListOfImports [matchImport NormalizedCurrentImport]
		by
			current [.CloneOutput]
end function


%match the import in the list
function matchImport	V1 [import_declaration]
	match * [import_declaration]
		currentImport [import_declaration]
	%first construct a string from it, and then compare the value
	construct stringV1 [stringlit]
		_ [quote V1]
	construct currentImportString [stringlit]
		_ [quote currentImport]
	where
		stringV1 [= currentImportString]
end function






%this function will match InterfaceHeader as clones, for similiarity percentage
%it will match every individual InterfaceHeaders line as clone, if it is contained in the original program,
%without considering any thrashold, but it will not output it as a clone
function matchClonedInterface	FirstProg [repeat interface_header]
								SecondProg [repeat interface_header]
	replace [repeat cloneOutput]
		current [repeat cloneOutput]
	deconstruct SecondProg 
		first [interface_header] rest [repeat interface_header]
	%add it as a clone, just if it is matched in the first program InterfaceHeader list
	construct newCurrentFields [repeat cloneOutput]
		current [addInterfaceIfMatched FirstProg first]
	by
		newCurrentFields
		[matchClonedInterface FirstProg rest]
end function


%match an InterfaceHeader in the list of the original program's InterfaceHeaders
function addInterfaceIfMatched	ListOfFields [repeat interface_header]
								CurrentField [interface_header]
		replace [repeat cloneOutput]
			current [repeat cloneOutput]
		construct CloneOutput [cloneOutput]
			CurrentField
		where 
			ListOfFields [matchInterface CurrentField]
		by
			current [.CloneOutput]
end function


%match the InterfaceHeader in the list
function matchInterface	V1 [interface_header]
	match * [interface_header]
		currentField [interface_header]
	%first construct a string from it, and then compare the value
	construct stringV1 [stringlit]
		_ [quote V1]
	construct currentFieldString [stringlit]
		_ [quote currentField]
	where
		stringV1 [= currentFieldString]
end function








%this function will match Class Header as clones, for similiarity percentage
%it will match every individual Class Header line as clone, if it is contained in the original program,
%without considering any thrashold, but it will not output it as a clone
function matchClassHeader	FirstProg [repeat class_header]
							SecondProg [repeat class_header]
	replace [repeat cloneOutput]
		current [repeat cloneOutput]
	deconstruct SecondProg 
		first [class_header] rest [repeat class_header]
	%add it as a clone, just if it is matched in the first program fields list
	construct newCurrentFields [repeat cloneOutput]
		current [addClassHeaderIfMatched FirstProg first]
	by
		newCurrentFields
		[matchClassHeader FirstProg rest]
end function


%match an Class header in the list of the original program's Class Headers
function addClassHeaderIfMatched	ListOfFields [repeat class_header]
									CurrentField [class_header]
		replace [repeat cloneOutput]
			current [repeat cloneOutput]
		construct CloneOutput [cloneOutput]
			CurrentField
		where 
			ListOfFields [matchHeader CurrentField]
		by
			current [.CloneOutput]
end function


%match the ClassHeader in the list
function matchHeader	V1 [class_header]
	match * [class_header]
		currentField [class_header]
	%first construct a string from it, and then compare the value
	construct stringV1 [stringlit]
		_ [quote V1]
	construct currentFieldString [stringlit]
		_ [quote currentField]
	where
		stringV1 [= currentFieldString]
end function








%this function will match fields as clones, for similiarity percentage
%it will match every individual field line as clone, if it is contained in the original program,
%without considering any thrashold, but it will not output it as a clone
function matchClonedField	FirstProg [repeat field_declaration]
							SecondProg [repeat field_declaration]
	replace [repeat cloneOutput]
		current [repeat cloneOutput]
	deconstruct SecondProg 
		first [field_declaration] rest [repeat field_declaration]
	%add it as a clone, just if it is matched in the first program fields list		
	construct newCurrentFields [repeat cloneOutput]
		current [addFieldIfMatched FirstProg first]	

	by
		newCurrentFields
		[matchClonedField FirstProg rest]
end function


%match an field in the list of the original program's fields
function addFieldIfMatched	ListOfFields [repeat field_declaration]
							CurrentField [field_declaration]
		replace [repeat cloneOutput]
			current [repeat cloneOutput]
		construct CloneOutput [cloneOutput]
			CurrentField
		%normalize before matching
		construct NormalizedListOfFields [repeat field_declaration]
			ListOfFields [normalize] 
		construct NormalizedCurrentField [field_declaration]
			CurrentField [normalize]
		where 
			NormalizedListOfFields [matchField NormalizedCurrentField]
		by
			current [.CloneOutput]
end function


%match the field in the list
function matchField	V1 [field_declaration]
	match * [field_declaration]
		currentField [field_declaration]
	%first construct a string from it, and then compare the value
	construct stringV1 [stringlit]
		_ [quote V1]
	construct currentFieldString [stringlit]
		_ [quote currentField]
	where
		stringV1 [= currentFieldString]


end function


%generate the list of all clones LOCs from the classofClones repeat, by ignoring all additional output for layout
%this will be used in line counter script
function addJustLinesFromClones	ListOfClasses [repeat classOfClones]
	replace [repeat cloneOutput]
		current [repeat cloneOutput]
	deconstruct ListOfClasses
		first [classOfClones] rest [repeat classOfClones]
	%take just the repeat of clone output
	deconstruct * [repeat cloneOutput] first
		justClones [repeat cloneOutput]
	%remove any additional output
	construct removedAdditionalOuput [repeat cloneOutput]
		justClones [removeFileMarker]
	by
		current [.removedAdditionalOuput]
		[addJustLinesFromClones rest]
		
end function


%this method will replace all cloneOutput, with the version without file name, source file and end marker
rule removeFileMarker
	replace $[cloneOutput]
		fn [opt fileMarker] srcFile[opt srcfilename] methSt[method_or_repeat_statements] endM[opt endMarker] 
		lineNo[opt srclinenumber] 
	construct newCloneOutput [cloneOutput]
		methSt lineNo
	by
		newCloneOutput
end rule




%just normalize all ids to x
rule normalize
	replace [id]
		Id[id]
	deconstruct not Id
		'x
	by
		'x
 end rule