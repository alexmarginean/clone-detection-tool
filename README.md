# This is my README

For testing it follow the next steps:

1) Run make for compiling the TXL programs. The command is: make
2) Two files with .x extension will be created. The type-1 is for type 1 clones, and type-2
is for type 2 clones.
3) For running the programs run the following commands:
	./CloneDetector-type"1/2".x firstFile secondFile

	A concrete run for the examples that I've created is:
	
	./CloneDetector-type1.x input/testJava.CloneDetector input/ModifiedVers.CloneDetector
	./CloneDetector-type2.x input/testJava.CloneDetector input/ModifiedVers.CloneDetector 

4) The output of the two programs is in folder output/ . For every of them, there will be 
one output: type1.out and type2.out.

5) At the next commit I will ignore inputs and outputs, but for the moment I left them
like this, for being more clear.

For the moment the tool is doing clone detection at function level. I will extend it to
block level soon. Also I will integrate tools for different type of clones in a C program,
so the command for calling it will be like:

	./CloneDetector -type "number" File1 File2 