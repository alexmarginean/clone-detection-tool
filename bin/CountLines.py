from __future__ import division
import sys


#this class will count the percentage of similiarity
#between two programs, as the number of LOCs that where identified
#in any clone, over the total LOCs of the program
class LineCounter:
    
    #init method, which receives the original program filename, and the clone
    #filename
    def __init__(self, filename,clonePath):
        self.FileName = filename
        self.CloneFileName = clonePath
    
    
    #this method will return the number of common lines between two lists of strings
    def countCommonLines(self, firstFile, secondFile):
        count = 0
        #because txl cannot parse " as stringlit, and some of the clones replaced
        #quotes sign with ', and others (functions or classes) not, first replace
        #all quotes marks from the clone output with '
        for j in range (0, len(secondFile)):
            secondFile[j] = secondFile[j].replace('"', "'" )
        for line in firstFile:
            try:
                #try if the line from the source clode was matched in a clone
                #since in the clone output the quotes where replaced by
                #', replace it in the source code too.
                replacedQutesLine = line.replace('"', "'" )
                pos = secondFile.index(replacedQutesLine)
                secondFile.remove(replacedQutesLine)
                count = count + 1
                line = ""
            except ValueError:
                z = ValueError
        return count
    
    
    #this method returns the percentage of similiarity
    #it will read all the lines from the both files and remove spaces, tabes and new
    #line signs
    #after this, it will call the previous method for counting the common lines
    #and then will return the percentage of similiarity
    def countLines(self):
        f1 = open(self.FileName)
        lines1 = f1.readlines()
        newLineArray1 = []
        for line in lines1:
            newLine = line.strip(' \t\r\n').lstrip(' \t\n\r').rstrip(' \t\r\n')
            if newLine and newLine != "{" and newLine != "}":
                newLineArray1.append(newLine)
        f1.close()
        f2 = open(self.CloneFileName)
        lines2 = f2.readlines()
        newLineArray2 = []
        for line in lines2:
            newLine = line.strip(' \t\r\n').lstrip(' \t\n\r').rstrip(' \t\r\n')
            if newLine and newLine != "{" and newLine != "}":
                newLineArray2.append(newLine)
        f2.close()
        t = self.countCommonLines(newLineArray1, newLineArray2)
        return t / len(newLineArray1)


#entry point of the program
xx = LineCounter(str(sys.argv[1]),str(sys.argv[2]))
print(xx.countLines())
