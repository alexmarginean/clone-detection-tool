from __future__ import division
import sys
import os


class LineWithAddOrDel:
    def __init__(self, l, addDelList = None):
        self.Line = l
        self.AddDelList = []



#this class will compute the LCSs between two files, with the length over the threshold
class LCSClass:
    
    
    #init method that receives the source file, destination file, the minimal threshold and the maximum
    #distance between LOCs for type 3 clones
    def __init__(self, source, destination, originalDestination,originalSource, minThreshold, distThreshold):
        self.cloneSource = source
        self.cloneDestination = destination
        self.MinimalCloneThreshold = int(minThreshold)
        self.MaximDistanceType3 = int(distThreshold)
        self.OriginalDestination = originalDestination
        self.OriginalSource = originalSource
    
    
    #this method computes the LCS matrix
    def LCS(self, X, Y):
        m = len(X)
        n = len(Y)
        C = [[0 for j in range(n+1)] for i in range(m+1)]
        for i in range(1, m+1):
            for j in range(1, n+1):
                newX = self.deconstructNumberedLine(X[i-1])
                newY = self.deconstructNumberedLine(Y[j-1])
                if newX.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == newY.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r'):
                    C[i][j] = C[i-1][j-1] + 1
                else:
                    C[i][j] = max(C[i][j-1], C[i-1][j])
        return C
    
    
    #for comparison remove the line numbers, which are not the same even if the line is the same
    #return the line number for outputting it
    def deconstructNumberedLine(self, line):
        line = line.lstrip('0123456789')
        return line
    
    
    #this method will return the LCS between two list of strings, and also an array of line numbers
    def printDiff(self, C, X, Y, i, j, outputList, listOfLineNumber):
        #remove line number
        newX = self.deconstructNumberedLine(X[i-1])
        newY = self.deconstructNumberedLine(Y[j-1])
        lineNumber = Y[j-1].replace(newY,"")
        #if there is a line number add it to the line number array
        if lineNumber.isdigit:
            listOfLineNumber[j-1] = lineNumber
        #if the lines are equal, add them to the output, and continue for the next lines
        if i > 0 and j > 0 and newX.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == newY.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r'):
            outputList.append(LineWithAddOrDel(newY))
            return self.printDiff(C, X, Y, i-1, j-1,outputList,listOfLineNumber)
        #addition or deletion
        else:
            #addition
            if j > 0 and (i == 0 or C[i][j-1] >= C[i-1][j]):
                #if it is an addition, add the line in the output just if it is a bracket
                if newY.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == '{' or newY.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == '}':
                    outputList.append(LineWithAddOrDel(newY))
                else:
                    outputList.append(LineWithAddOrDel(""))
                    if Y[j-1].strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r'):
                        outputList[len(outputList) - 1].AddDelList.append("\"+ " + self.codeOriginalDestination[j-1].replace('"', "'" ) + "\"")
                return self.printDiff(C, X, Y, i, j-1, outputList,listOfLineNumber)
            #deletion
            elif i > 0 and (j == 0 or C[i][j-1] < C[i-1][j]):
                if len(outputList) > 0 and X[i-1].strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r'):
                    outputList[len(outputList) - 1].AddDelList.append("\"- " + self.codeOriginalSource[i-1].replace('"', "'" ) + "\"")
                return self.printDiff(C, X, Y, i-1, j, outputList,listOfLineNumber)
        return outputList
    
    
    #remove from the tested file the lines that were already returned as LCSs
    def removeConsideredLOCs(self, AllLines, UsedLines):
        assert len(AllLines) == len(UsedLines)
        for i in range (0, len(UsedLines)):
            if UsedLines[i].Line != "":
                AllLines[i] = "";
        return AllLines
    
    
    #read lines from files
    def readFromFile(self):
        #first file, the original program
        f1 = open(self.cloneSource)
        lines1 = f1.readlines()
        newLineArray1 = []
        for line in lines1:
            #add it just if it is not an empty line
            newLine = line.strip(' \t\r').lstrip(' \t\n\r').rstrip(' \t\r')
            if newLine:
                newLineArray1.append(newLine)
        f1.close()
        self.codeSource = newLineArray1
        #second program
        f2 = open(self.cloneDestination)
        lines2 = f2.readlines()
        newLineArray2 = []
        for line in lines2:
            newLine = line.strip(' \t\r').lstrip(' \t\n\r').rstrip(' \t\r')
            #add it just if it is not an empty line
            if newLine:
                newLineArray2.append(newLine)
        f2.close()
        self.codeDestination = newLineArray2
        #the cloned file with original identifiers, for outputting them as a clone
        f3 = open(self.OriginalDestination)
        lines3 = f3.readlines()
        newLineArray3 = []
        for line in lines3:
            newLine = line.strip(' \t\r').lstrip(' \t\n\r').rstrip(' \t\r')
            #add it just if it is not an empty line
            if newLine:
                newLineArray3.append(newLine)
        f3.close()
        self.codeOriginalDestination = newLineArray3
        #the main file with the original identifiers for output
        f4 = open(self.OriginalSource)
        lines4 = f4.readlines()
        newLineArray4 = []
        for line in lines4:
            newLine = line.strip(' \t\r').lstrip(' \t\n\r').rstrip(' \t\r')
            #add it just if it is not an empty line
            if newLine:
                newLineArray4.append(newLine)
        f4.close()
        self.codeOriginalSource = newLineArray4
        #reverse for matching with LCS print method
        self.codeOriginalSource.reverse()
    
    
    #this method will compute the actual length of a matched LCS
    def lengthOfOutput (self, listOfOutput):
        count = 0
        for line in listOfOutput:
            if line.Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != '{' and line.Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != '}' and line.Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != '':
                count = count + 1
        return count
    
    
    #this method will remove lines that are too far from previous ones
    #for type 1 and 2 the distance is 1, so the output will be just continuous LOCs
    #for type 3 it can be anything greater than one
    def removeToDistantiatedLOCs(self, OutputList, threshold):
        #start with the minimal int for count, so until the first value that is not 0
        #is reached, nothing is eliminated
        count = -32767
        removeMark = False
        for i in range (0, len(OutputList)):
            #if it the removeMark is true, and the lines are not brackets, just remove them from the output
            if removeMark and OutputList[i-1].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != '{' and OutputList[i-1].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != '}':
                OutputList[i - 1].Line = ""
            #else if the line is empty, it means that the line is not common
            #so increase the count
            elif OutputList[i].Line == "":
                count = count + 1
            #if the count is greater than the max dist threshold, it means that the rest of the clone cannot be treated as
            #the same clone, so mark the rest lines for removal
            elif count >= threshold:
                removeMark = True
            else:
                count = 0
        #separate case for the last line
        if removeMark and OutputList[len(OutputList) - 1].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != '{' and OutputList[len(OutputList) - 1].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != '}':
            OutputList[len(OutputList) - 1].Line = ""
    
    
    #since in the TXL programs outputs from here are treated as strings, the removal of empty blocks should be done
    #here
    def removeEmptyBlocks(self, listg):
        #if in this iteration at least one empty block was removed, it is possible that its parent
        #must be removed too, so mark this for a recursive call
        signal = 0
        for i in range (0, len(listg)):
            #process just the start of the block: {
            if listg[i].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == "{":
                j = i+1
                s = 0
                #while the end of the block was not matched, increase j
                while j < len(listg) and listg[j].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') != "}" and s == 0:
                    #if a non empty char was matched, then this is not an empty block
                    if listg[j].Line:
                        s = 1
                    j = j + 1
                #if no non-empty lines, then this is an empty block and it must be removed
                if s == 0:
                    listg [i].Line = ""
                    listg [j].Line = ""
                    signal = 1
        #if at least one non empty block was matched, call this method again
        if signal == 1:
            self.removeEmptyBlocks(listg)
    
    
    #this method will output all LCSs
    def outputDifferences(self):
        self.readFromFile()
        #output file for LCSs
        f = open("Temporary/output.out", "w")
        #reverse the lines, since the LCS algorithm starts backwards
        self.codeSource.reverse()
        self.codeDestination.reverse()
        #line numbers start with -1
        listOfLineNumbers = [-1 for i in range(0, len(self.codeDestination))]
        outputList =  self.printDiff(self.LCS(self.codeSource, self.codeDestination), self.codeSource, self.codeDestination, len(self.codeSource), len(self.codeDestination), [],listOfLineNumbers)
        #reverse the line number array, since the output list is reversed too
        listOfLineNumbers.reverse()
        #while the LCS length is greater than the threshold, try to find another LCS
        while self.lengthOfOutput(outputList) >= self.MinimalCloneThreshold:
            #remove lines that are too far for being considered as the same clone
            self.removeToDistantiatedLOCs(outputList, self.MaximDistanceType3)
            #now check if the length is still greater than the threshold
            if self.lengthOfOutput(outputList) >=self.MinimalCloneThreshold:
                self.removeEmptyBlocks(outputList)
                f.write("\nBEGINCLONE\n")
                initialPos = 0
                while outputList[initialPos].Line == "" or outputList[initialPos].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == "{" or outputList[initialPos].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == "}":
                    initialPos = initialPos + 1
                finalPos = len(outputList) - 1
                while outputList[finalPos].Line == "" or outputList[finalPos].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == "{" or outputList[finalPos].Line.strip(' \t\n\r').lstrip(' \t\n\r').rstrip(' \t\n\r') == "}":
                    finalPos = finalPos - 1
                #write in the output file
                #since TXL cannot read quotes marks as a part of a stringlit, just replace them with '
                for k in range(0, len(outputList)):
                    if outputList[k].Line != "":
                        #output the version in the original file, with the original identifiers!
                        f.write("\"" + self.codeOriginalDestination[k].replace('"', "'" ) + "\"")
                    if outputList[k].AddDelList and k > initialPos and k < finalPos:
                        for lineAddDel in outputList[k].AddDelList:
                            f.write(lineAddDel)
                f.write("\nENDCLONE\n")
            #reverse for being the same order as clone file code, and then remove the already considered lines
            outputList.reverse()
            self.removeConsideredLOCs(self.codeDestination, outputList)
            listOfLineNumbers = []
            #match a new LCS
            listOfLineNumbers = [-1 for i in range(0, len(self.codeDestination))]
            outputList =  self.printDiff(self.LCS(self.codeSource, self.codeDestination), self.codeSource, self.codeDestination, len(self.codeSource), len(self.codeDestination), [],listOfLineNumbers)
            listOfLineNumbers.reverse()
        f.close()


#the entry point of the program
xx = LCSClass(str(sys.argv[1]),str(sys.argv[2]), sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6] )
xx.outputDifferences()