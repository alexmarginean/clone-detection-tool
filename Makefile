all:
	txlc CloneDetectorInstantiatedIDS.txl
	txlc CloneDetectorAbstractIDS.txl
	mv CloneDetectorInstantiatedIDS.x bin/
	mv CloneDetectorAbstractIDS.x bin/
	cc -o CloneDetectorMain -std=c99 main.c
	cc -o TestHarnessMain -std=c99 TestHarness.c

clean:
	-rm -f bin/CloneDetector-type1.x
	-rm -f bin/CloneDetector-type2.x
	-rm -f CloneDetectorMain
	-rm -f TestHarnessMain

cleanTemp:
	-rm -f Temporary/*.*

cleanOut:
	-rm -f output/*.*